const express = require('express');
const router = express.Router();
const hardwareMap = require('../mapping/hardwareMap');
const tempHumid = require('../controllers/tempHumid.controller');
const jwt_controller = require('../controllers/jwt.controller');

router.post('/get', jwt_controller, (req, res) => {
    const id = req.body.plant_id;
    tempHumid(hardwareMap[id].tempPin, (err, stats) => {
        if(err) res.status(500).send({message: 'Internal error!'});
        res.status(200).send(stats);
    });
});

module.exports = router;