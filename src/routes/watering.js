const express = require('express');
const router = express.Router();
const hardwareMap = require('../mapping/hardwareMap');
const jwt_controller = require('../controllers/jwt.controller');
const wateringController = require('../controllers/watering.controller');

router.post('/watering-start', jwt_controller, (req, res) => {
    let plant_id = req.body.plant_id;
    wateringController.startWatering(plant_id, (err, message) => {
        if (err) res.status(500).send(err);
        res.status(200).send(message);
    });
});

module.exports = router;