class hardwareSet {
    constructor(wateringPin, TempHumidPin) {
        this.wateringPin = wateringPin;
        this.tempPin = TempHumidPin;
        this.humidPin = TempHumidPin;
    }
}

let set1 = new hardwareSet(17, 4);
let set2 = new hardwareSet(27, 4);
let set3 = new hardwareSet(22, 4);

module.exports = [
    set1,
    set2,
    set3,
];