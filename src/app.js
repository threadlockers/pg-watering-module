const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const initSensors = require('./controllers/initSensors.controller');

const wm = express();

wm.use(bodyParser.json());
wm.use(bodyParser.text());
wm.use(cors());

initSensors();

const port = process.env.PORT || 80;

const watering = require('./routes/watering');
const stats = require('./routes/stats');

wm.use('/watering', watering);
wm.use('/stats', stats);

wm.listen(port, '0.0.0.0', () => {
    console.log(`Watering Module Running on port ${port}`);
});