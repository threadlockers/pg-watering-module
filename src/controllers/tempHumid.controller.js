const dhtSensor = require('rpi-dht-sensor');
const hardwareMap = require('../mapping/hardwareMap');

class responseStats {
    constructor(temperature, humidity) {
        this.temperature = temperature;
        this.humidity = humidity;
    }
}

function getStats (pinNumber, callback) {
    let dht = new dhtSensor.DHT11(pinNumber);
    let readout = dht.read();
    let response = new responseStats(readout.temperature,
                                    readout.humidity);
                                    console.log('Temperature: ' + readout.temperature.toFixed(2) + 'C, ' +
                                    'humidity: ' + readout.humidity.toFixed(2) + '%');
    callback(null, response);
}

module.exports = getStats;