const tempHumid = require('./tempHumid.controller');
const hardwareMap = require('../mapping/hardwareMap');

function init () {
    for(var index = 0; index < hardwareMap.length; index++) {
        
        this.index = index;
        let that = this;
        
        function timeoutMethod () {
            tempHumid(hardwareMap[that.index].tempPin, (err, status) => {
                    console.log(`Initialized sensor ${that.index}`);
                });
        }
        setTimeout(timeoutMethod, 250);
    }
}


module.exports = init;