const hardwareMap = require('../mapping/hardwareMap');
const Gpio = require('onoff').Gpio;
let pumpArray = new Array();

console.log(hardwareMap[0]);

for (let pinConf of hardwareMap) {
    pumpArray.push(new Gpio(pinConf.wateringPin, 'out'));
}

function startWatering(plant_id, callback) {
    pumpArray[plant_id].read((err, value) => {
        if (err) throw err;
        if(value == 0)
            pumpArray[plant_id].write(1, (err) => {
                if (err) callback(err);
                setTimeout(function () { stopWatering(plant_id); }, 2000);
                callback(null, { message: 'Watering started!' });
            });
    });
}

function stopWatering(plant_id) {
    pumpArray[plant_id].write(0, (err) => {
        if (err) throw err;
        console.log(`Stopped watering on plant ${plant_id} with pin number ${hardwareMap[plant_id].wateringPin}`);
    });
}

module.exports = {
    startWatering
}